You should smile more.
Don't cry because it's over, smile because it happened.
Peace begins with a smile.
I was smiling yesterday, I am smiling today and I will smile tomorrow. Simply because life is too short to cry for anything.
I like your smile.
Sometimes your joy is the source of your smile, but sometimes your smile can be the source of your joy.
If I saw you hitchhiking, I'd smile and return your thumb's up, just for you doing such a great job of being a positive roadsite influence.
Let us always meet each other with a smile, for the smile is the beginning of love.
But, my God, it's so beautiful when you smile.
Smile like you can't help it.
You'll find that life is still worthwhile, if you just smile.
Smile when you are alone, and you'll really mean it.
Your smile can change the world.
Smile at strangers and you'll improve their life.
The world is like a mirror; frown at it, and it frowns at you. Smile and it smiles, too.
Nothing shakes the smiling heart.
You can go a long way with a smile.
Have a smile for everyone you meet.
You've got to S-M-I-L-E to be H-A-Double-P-Y!
If you've got nothing to dance about, find a reason to sing.
The smile upon my lips, It will surely never die.
You shouldn't regret something that made you smile.
Days may smile and may not. But you can smile anytime, just smile.
There is a thin line between smile and laughter.
An enigmatic smile is worth ten pages of dialog.
Science teaches to think but love teaches to smile.
Cheer up.
You have a pretty smile.
Turn that frown upside down!
Don't worry, be happy!
You send my soul sky high when your smilin' starts
Your smile takes the grey skies out of my way
Your smile makes the sun shine brighter than on Doris Day
Your smile turns a bright spark into a flame.
The world is young, and your smile is so beautiful.
Can I see you smile?
Recreation trains your body and reading trains your mind. But smiling trains your spirit.
Don't have a frown, have a smile!
I would love to see you smile.
You are a wonderful, inspiring person. Smile!
Smiling is contagious. Spread the contagion!
You're never fully dressed without a smile!
Your true beauty shines through when you smile.
Smile like you mean it!
A smile can show what a thousand words can't describe.
Spread the joy and laughter. It all starts with a smile.
It takes 43 muscles to frown, but only 17 to smile.
A smile can turn a bad day into a good day. 