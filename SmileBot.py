import praw;
import sys;
from random import randint

# setup
user_agent = ("SmileBot 1.0 by /u/Fimoreth")
r = praw.Reddit(user_agent);
user_name = "Smile_Bot";
happy = u":)";
sad = u":(";    # adding spaces in event of it being code

# Read from file
with open("smile.txt") as f:
    content = f.readlines();

# login
r.login();
print("Connected.");

# first, gather list of submissions already replied to
already_done = [];
me = r.get_redditor(user_name);
for comment in me.get_comments():
    already_done.append(comment.parent_id[3:])
    print(comment.parent_id[3:]);
print("--------");
# loop
while (1):
    #comments = r.get_comments("LittleSecretPath");#('LittleSecretPath');
    #comments = r.get_comments("AdviceAnimals+gaming+pics+funny+askreddit+aww+gifs+iama+botcirclejerk");
    comments = r.get_comments("all");
    for comment in comments:
        if sad in str(comment.body.encode('ascii', 'ignore')):
            if not comment.id in already_done:
                the_reply = ">" + content[randint(0,len(content)-1)][:-1] + "\n\n" + happy;
                comment.reply(the_reply);
                already_done.append(comment.id);
                print("Replied to " + comment.id + ", " + the_reply);


input("Bot over..");
